using UnityEngine.Audio;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AudioHandler : MonoBehaviour
{
    public Sound[] sounds;
    public String mainsong = "Voices";
    private bool stillSilence = true;
    private AudioPeer peer;
    public bool playOnAwake = false;

    // initialization
    void Awake()
    {
        peer = GetComponent<AudioPeer>();
        foreach (Sound s in sounds)
        {
            // save Audiosource in sound object
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }

        if (playOnAwake)
        {
            Play(mainsong);
        }
    }
    public void Play(string name)
    {
        if (name == mainsong)
        {
            //main song nur einmal abspielen, wenn material swap stattfindet (evtl mehrfach)
            if (stillSilence)
            {
                stillSilence = false;
                //finde sound mit entsprechendem Namen und play
                Sound s = Array.Find(sounds, sound => sound.name == name);
                s.source.Play();
                //enable audiopeer
                if (peer) peer.enabled = true;
                //peer.audioSource = s.source;
            }
        }
        else
        {
            //finde sound mit entsprechendem Namen und play
            Sound s = Array.Find(sounds, sound => sound.name == name);
            s.source.Play();
        }

    }

    public IEnumerator StartFade(string name, float duration, float targetVolume)
    {

        Sound s = Array.Find(sounds, sound => sound.name == name);
        float currentTime = 0;
        float start = s.source.volume;
        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            s.source.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        yield break;
    }
}
