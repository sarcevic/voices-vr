using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(Animator))]
public class Hand : MonoBehaviour
{
    public Material gridMaterial;
    public float speed;
    public GameObject lefthand;
    public GameObject righthand;
    Animator animator;
    private float gripTarget;
    private float triggerTarget;
    private float gripCurrent;
    private float triggerCurrent;
    private string animatorGripParam = "Grip";
    private string animatorTriggerParam = "Trigger";
    public VisualEffectAsset destroyEffect;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void SetGrip(float v)
    {
        gripTarget = v;
    }

    internal void SetTrigger(float v)
    {
        triggerTarget = v;
    }

    void AnimateHand()
    {
        if (gripCurrent != gripTarget)
        {
            gripCurrent = Mathf.MoveTowards(gripCurrent, gripTarget, Time.deltaTime * speed);
            animator.SetFloat(animatorGripParam, gripCurrent);
        }
        if (triggerCurrent != triggerTarget)
        {
            triggerCurrent = Mathf.MoveTowards(triggerCurrent, triggerTarget, Time.deltaTime * speed);
            animator.SetFloat(animatorTriggerParam, triggerCurrent);
        }
    }

    void OnCollisionEnter(Collision targetObject)
    {
        if (targetObject.gameObject.tag == "interacted")
        {
            //Debug.Log(this.gameObject.name + " "+targetObject.gameObject.name);
            //change material: hands -> collided object
            lefthand.GetComponent<SkinnedMeshRenderer>().material = targetObject.gameObject.GetComponent<MeshRenderer>().material;
            righthand.GetComponent<SkinnedMeshRenderer>().material = targetObject.gameObject.GetComponent<MeshRenderer>().material;


            Color collisionGlowColor = targetObject.gameObject.GetComponent<MeshRenderer>().material.GetColor("glowColor");
            gridMaterial.SetColor("gridColor", collisionGlowColor);




            //TODO explotion prefab
            //ContactPoint contact = collision.contacts[0];
            //Quaternion rotation = Quaternion.FromToRotation(Vector3.up, contact.normal);
            //Vector3 position = contact.point;
            //Instantiate(explosionPrefab, position, rotation);
            //Destroy(gameObject);
            //abgewehrtes Objekt zerstören

            GameObject effectObj = new GameObject();
            VisualEffect effect = effectObj.AddComponent<VisualEffect>();
            effect.visualEffectAsset = destroyEffect;
            effect.SetVector4("Color", collisionGlowColor);
            effectObj.transform.position = targetObject.transform.position;

            Destroy(targetObject.gameObject);

        }

        else
        {
            //Debug.Log(this.gameObject.name + " " + targetObject.gameObject.name + "  hand collision. not interactable object");

        }
    }
}
