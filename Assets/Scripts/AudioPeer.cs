using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.Interaction.Toolkit;

public class AudioPeer : MonoBehaviour
{
    AudioSource audioSource;
    List<GameObject> schwarmObjekte = new List<GameObject>();
    public float[] samples = new float[2];
    bool beforeDrop = true;
    private int objectcounter = 0;
    public GameObject desert;
    public Material newDesertGrid;
    public Color desertColor;
    public Animator fadeAnimator;
    public AudioHandler audioHandler;
    private float baseHoloness = 1;
    public Material holostones;
    public float holonessMultiplier = 1.0F;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        //Debug.Log("start audiopeer");
        // not just for prototyping
        StartCoroutine(StartMovingToPlayer());
        newDesertGrid.SetColor("gridColor", desertColor);
        //baseHoloness = holostones.GetFloat("holoness");
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(schwarmObjekte.Count);
        //audioSource.GetSpectrumData(samples, 0, FFTWindow.Blackman);
        audioSource.GetOutputData(samples, 0);
        float newHoloness = baseHoloness + holonessMultiplier * Math.Abs(samples[0]);
        Debug.Log(samples[0]);
        schwarmObjekte.ForEach(gameObj =>
        {
            if (gameObj == null) return;
            gameObj.GetComponent<MeshRenderer>().material.SetFloat("holoness", newHoloness);
        });

        if (!beforeDrop)
        {
            //beforeDrop = !dropper(samples);
            if (schwarmObjekte.Count - objectcounter == 0)
            {
                //load Scene
                StartCoroutine(waitForCredits());
            }
            if (schwarmObjekte.Count - objectcounter > 0)
            {
                //das erste objekt fliegt zuerst los
                GameObject removedObject = schwarmObjekte[objectcounter];
                removedObject.GetComponent<MeshCollider>().enabled = true;
                removedObject.GetComponent<PositionController>().movingToPlayer = true;
            }
            //if (schwarmObjekte.Count-objectcounter == 1)
            //{
            //    //letztes objekt bzw einziges
            //    GameObject removedObject = schwarmObjekte[objectcounter];
            //    removedObject.GetComponent<PositionController>().movingToPlayer = true;
            //}
            beforeDrop = true;
            objectcounter++;
        }
        //Debug.Log(samples[0]);
    }

    IEnumerator waitForCredits()
    {
        StartCoroutine(audioHandler.StartFade("Voices", 3, 0));
        yield return new WaitForSeconds(2);
        fadeAnimator.enabled = true;
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Credits");
    }



    //private bool dropper(float[] sams)
    //{
    //    if (sams[0] > 0.6)
    //    {
    //        Debug.Log(sams[0]);
    //        return true;
    //    }    
    //    return false; //TODO
    //}

    public void addInteractedObject(GameObject flyingObject)
    {
        schwarmObjekte.Add(flyingObject);
    }

    IEnumerator StartMovingToPlayer()
    {
        yield return new WaitForSeconds(13); // wait til drop
        FindObjectOfType<AudioHandler>().Play("PunchStone");
        this.SwapDesertMaterial();
        this.tagInteractedObjects(); // give objects in swarm a new tag
        this.destroyInteractables(); //destroys all interactable object that are not in the swarm
        beforeDrop = false; // can just be set via event or script controller or something else
        for (int i = 0; i < schwarmObjekte.Count; i++)
        {
            yield return new WaitForSeconds(2); // wait for next rock
            beforeDrop = false;
        }
    }

    private void tagInteractedObjects()
    {
        foreach (GameObject swarmObj in schwarmObjekte)
        {
            swarmObj.tag = "interacted";
        }
    }

    private void destroyInteractables()
    {
        GameObject[] gos = GameObject.FindGameObjectsWithTag("interactableObject");
        foreach (GameObject go in gos)
        {
            go.GetComponent<XRGrabInteractable>().enabled = false;
            go.GetComponent<Renderer>().material = newDesertGrid;
        }
            
            //Destroy(go);
    }

    private void SwapDesertMaterial()
    {
        MeshRenderer[] rends;
        rends = desert.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer rend in rends)
        {
            // get the current (original) materials instances
            // Renderer.materials returns a new copy of the array every time
            var materials = rend.materials;

            // in the local array simply replace all elements
            for (var i = 0; i < materials.Length; i++)
            {
                materials[i] = newDesertGrid;
            }

            // assign back the entire materials array
            rend.materials = materials;
        }
    }
}
