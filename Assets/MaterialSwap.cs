using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSwap : MonoBehaviour
{

    public Material newMaterial;
    public Color baseColor;
    public Color holoColor;
    public Color glowColor;
    
    
    

    void Start()
    {
        FindObjectOfType<AudioHandler>().GetComponent<AudioPeer>().addInteractedObject(this.gameObject);

        //var random = new Random();
        Material instanzMaterial = new Material(newMaterial);
        instanzMaterial.SetColor("baseColor", baseColor);
        instanzMaterial.SetColor("holoColor", holoColor);
        instanzMaterial.SetColor("glowColor", glowColor);
        // Ersetzen des Materials
        GetComponent<MeshRenderer>().material = instanzMaterial;
        
    }

}















