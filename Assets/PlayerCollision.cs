
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCollision : MonoBehaviour
{

    public Material gridMaterial;
    public Color gridColor;
    private int collisionCount = 0;
    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(this.gameObject.name + "," +collision.gameObject.name + " tag:" + collision.gameObject.tag);
        
        if (collision.gameObject.tag == "interacted")
        {


            //change grid color
            //Debug.Log("Player collision");

            Color curCol = gridMaterial.GetColor("gridColor");
            Color darker = new Color(curCol.r * .125f, curCol.g * .125f, curCol.b * .125f);
            gridMaterial.SetColor("gridColor", darker);

            collisionCount++;
            //destroy object
            Destroy(collision.gameObject);

            if (collisionCount > 1)
            {
                SceneManager.LoadScene("GameOver");
                //Debug.Log("GameOver");
            }
            
            FindObjectOfType<AudioHandler>().Play("LastChance");

        }
        else
        {
            //Debug.Log("player collision with non interactable object");
        }


    }
}
