using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public float timeToRestart = 15;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitForRestart());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator waitForRestart()
    {
        yield return new WaitForSeconds(timeToRestart);
        SceneManager.LoadScene("SampleScene");

    }
}
