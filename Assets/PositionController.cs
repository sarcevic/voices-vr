using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PositionController : MonoBehaviour
{

    public Transform cameraTransform;

    // Abstand zum Spieler / Kamera
    public float distanceFromCamera = 50;
    // Fixierte H�he des Objekts
    public float height = 10;
    // Anteiligegeschwindigkeit f�r Lerp
    public float catchUpSpeed = 0.8f;
    


    public bool movingToPlayer = false;
    public float attackSpeed = 1f;

    private Vector3 velocity = Vector3.zero;
    public Vector3 rotationAxis = new Vector3(0.0f, 0.2f, 0.0f); //make this a bit random
    private Vector3 objectPosition;
    public float radius = 10; //make this a bit random
    public float angle = 90; //make this a bit random

    private Vector3 prevPivot; //speichert den punkt, um den rotiert wird für lerp
    private bool beginRotation = false;

    [SerializeField] private UnityEvent m_PlayerHit;

    private void Start()
    {
        prevPivot = cameraTransform.position + cameraTransform.forward * distanceFromCamera;
        prevPivot.Set(prevPivot.x, height, prevPivot.z);
        if (m_PlayerHit == null)
        {
            m_PlayerHit = new UnityEvent();
        }

        GetComponent<Rigidbody>().mass = 0;
        GetComponent<Rigidbody>().useGravity = false;

        // Start der Musik
        FindObjectOfType<AudioHandler>().Play("Voices");

    }

    void Update()
    {
        if (!movingToPlayer)
        {
            // Berechnen der neuen Position
            Vector3 resultingPosition = cameraTransform.position + cameraTransform.forward * distanceFromCamera;
            resultingPosition.Set(resultingPosition.x, height, resultingPosition.z);

            //distanz zur position bestimmen
            float distance = Vector3.Distance(cameraTransform.position, transform.position);

            

            // Anwenden der neuen Position mittels Lerp catchUpSpeed
            prevPivot = Vector3.Lerp(prevPivot, resultingPosition, catchUpSpeed * Time.deltaTime);

            //check: bereit für rotation oder muss sich noch weiter entfernen?
            if (!beginRotation && distance < distanceFromCamera - radius*1.1) //Bewegung in Richtung des Schwarms
            {
                transform.position = prevPivot;
            }
            else //Rotation
            {
                beginRotation = true;
                //bestimmen der Objektposition ( damit Radius berücksichtigt wird, Radius kann nicht rotateAround-Funktion übergeben werden)
                objectPosition = radius * Vector3.Normalize(this.transform.position - prevPivot) + prevPivot;
                this.transform.position = objectPosition;
                //Rotation um den punkt "prevPivot" und der Rotationsachse "rotationAxis" und dem Winkel "angle"
                transform.RotateAround(prevPivot, rotationAxis, angle * Time.deltaTime);
            }

            
        }
        else
        {
            //float distance = Vector3.Distance(cameraTransform.position, transform.position);
            //Debug.Log(distance);
            //if (distance <= 0.1f)
            //{
            //    m_PlayerHit.Invoke();
            //    gameObject.SetActive(false);
            //}
            //else
            //{
            transform.position = Vector3.SmoothDamp(transform.position, cameraTransform.position, ref velocity, attackSpeed);
            
            //}
        }
    }

    
}

















